Reduced node edit
*****************

Description
***********

Reduced node edit lets users edit a node even if they don't have access to the
body's input format: in that situation, the body field will be hidden, but the
user will have their normal edit access to the rest of the node. A good
description of the problem is in issue #91663[1]:

> Users lose (or don't have) edit access to nodes that they should be able to
> edit, given their roles and associated access control settings.
>
> The node.module node_access() function denies 'update' access to a user, if a
> node has an input filter assigned that the user cannot access.
>
> This can be a problem because a user can create a node, the administrator may
> alter input filter permissions so that the input filter used to create the
> node is no longer accessible to the user (or edit the node and assign a
> restricted input filter to the node) - and the original user can no longer
> edit a node (the 'edit' tab disappears from the node menu, and if the user
> tries to access the edit function via /node/x/edit URL, the users receives an
> access denied message.)

This module will hide the body textarea in those cases where the user doesn't
have access to the input format; they can still edit the title and other fields
that they *do* have access to.

Installation & Use
******************

This is a fire-and-forget module: just install it the normal way[2] and if you
visit a node as a user who has edit permissions for the node, but no access to
the input format, you should still see the 'Edit' tab, which will give you the
reduced node edit form (ie without the body text area). This is the only
situation where this module will modify the node edit form.

Known issues
************

This module works by altering the access callback for the edit tab, so is
designed to work when you edit a node via the path node/*/edit. It doesn't (and
can't) alter node_access itself, and so any functionality that calls that
function directly will still act as if the user doesn't have permission.

Notes
*****

I've tried to ensure that this module will play nicely with other modules that
may also try to alter the menu callback for the 'Edit' tab. It tries to
transparently wrap the existing access callback, just removing the input format
as a possible cause for denying access. If it can't do this it will switch
itself off (and give a watchdog error).

Possible future development
***************************

* Add support for choosing which content types offer reduced node edit
  functionality.

* Support CCK fields with input formats.

[1] http://drupal.org/node/91663
[2] http://drupal.org/documentation/install/modules-themes/modules-5-6

author: AndyF
http://drupal.org/user/220112